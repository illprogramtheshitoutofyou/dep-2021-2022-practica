{-|
    Module      : Lib
    Description : Checkpoint voor V2DEP: recursie en lijsten
    Copyright   : (c) Brian van de Bijl, 2020
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum oefenen we met het schrijven van simpele functies in Haskell.
    Specifiek leren we hoe je recursie en pattern matching kunt gebruiken om een functie op te bouwen.
    LET OP: Hoewel al deze functies makkelijker kunnen worden geschreven met hogere-orde functies,
    is het hier nog niet de bedoeling om die te gebruiken.
    Hogere-orde functies behandelen we verderop in het vak; voor alle volgende practica mag je deze
    wel gebruiken.
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- TODO: Schrijf en documenteer de functie ex1, die de som van een lijst getallen berekent.
-- Voorbeeld: ex1 [3,1,4,1,5] ~> 14
ex1 :: [Int] -> Int
ex1 = som
-- The base cae returns 0 if all elements are added. The zero makes sure it doesnt impact the result.
som [] = 0
-- The recursion adds the first element of the list with the same function given the tail of the list. This way all elements are added one for one.
som (x:xs) = x + som(xs)

-- TODO: Schrijf en documenteer de functie ex2, die alle elementen van een lijst met 1 ophoogt.
-- Voorbeeld: ex2 [3,1,4,1,5] ~> [4,2,5,2,6]
ex2 :: [Int] -> [Int]
ex2 = eenhoog
-- This function adds 1 for the first element in the list. Than it does the same given the tail of the list. 
-- However, if the list is empty, the last element is concatenated, as well as the empty list to combine all elements into a list.
-- One way is to use if-then\else in order to establish the base case. See code below:
-- -- eenhoog (x:xs) = if xs==[]
-- --                then x+1:([])
-- --                else x+1:(eenhoog xs)
eenhoog (x:[]) = x+1:([])
eenhoog (x:xs) = x+1:(eenhoog xs)

-- TODO: Schrijf en documenteer de functie ex3, die alle elementen van een lijst met -1 vermenigvuldigt.
-- Voorbeeld: ex3 [3,1,4,1,5] ~> [-3,-1,-4,-1,-5]
ex3 :: [Int] -> [Int]
ex3 = multiply_neg
--  The base case makes sure no elements are added once the list is empty. It only returns the empty list to be concatenated to create a list.
multiply_neg [] = []
-- The recursion multiplies the first element of the given list with -1 and adds the negative first element of the tail of the list. 
multiply_neg (x:xs) = -x:(multiply_neg(xs))

-- TODO: Schrijf en documenteer de functie ex4, die twee lijsten aan elkaar plakt.
-- Voorbeeld: ex4 [3,1,4] [1,5] ~> [3,1,4,1,5]
-- Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. 
-- Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
ex4 :: [Int] -> [Int] -> [Int]
ex4 = conc2
-- first we define a function that concats a list to a character. Apparantly this function could be done easier, however this is how I came up with the answer.
-- -- conc x [] = x:[]
-- -- conc x (y:ys) = x: (conc y ys)
-- Now we extend the function that concatenates 2 lists. First we need a basecase
-- The base case tells us that whenever the recursion through the first list is over, we return the second list
conc2 [] ys = ys
-- In the recursion we take the first element of list1, and recall the function on the tail of list1. So the same way you loop through one list.
conc2 (x:xs) ys = x:(conc2 xs ys )


-- TODO: Schrijf en documenteer een functie, ex5, die twee lijsten van gelijke lengte paarsgewijs bij elkaar optelt.
-- Voorbeeld: ex5 [3,1,4] [1,5,9] ~> [4,6,13]
ex5 :: [Int] -> [Int] -> [Int]
ex5 = sumLsts
-- Our base case tells us that the recursion stops if no elements are left, and we return an empty list that combine all elements into a list.
sumLsts [] [] = []
-- The recursion adds the first elements of both lists, and does the same given the tails of the lists as input. 
sumLsts (x:xs) (y:ys) = (x+y):(sumLsts xs ys)


-- TODO: Schrijf en documenteer een functie, ex6, die twee lijsten van gelijke lengte paarsgewijs met elkaar vermenigvuldigt.
-- Voorbeeld: ex6 [3,1,4] [1,5,9] ~> [3,5,36] 
ex6 :: [Int] -> [Int] -> [Int]
ex6 = multiLsts
-- Our base case tells us that the recursion stops if no elements are left, and we return an empty list that combine all elements into a list.
multiLsts [] [] = []
-- The recursion multiplies the first elements of both lists, and does the same given the tails of the lists as input. 
multiLsts (x:xs) (y:ys) = (x*y):(multiLsts xs ys)

-- TODO: Schrijf en documenteer een functie, ex7, die de functies ex1 en ex6 combineert tot een functie die het inwendig product uitrekent.
-- Voorbeeld: ex7 [3,1,4] [1,5,9] geeft 3*1 + 1*5 + 4*9 = 44 terug als resultaat.

-- The innerproduct is can be calculated by applying function 1 to function 6. Function 1 calculates the sum of a list, function 6 calculates the 
-- product of all elements in a list. Together it calculates the inner product. 
ex7 :: [Int] -> [Int] -> Int
ex7 = innerProduct
innerProduct xs ys = ex1 $ ex6 xs ys

