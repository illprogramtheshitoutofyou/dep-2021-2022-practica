{-|
    Module      : Lib
    Description : Checkpoint voor V2DeP: cellulaire automata
    Copyright   : (c) Brian van der Bijl & Nick Roumimper, 2020
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum gaan we aan de slag met 1D cellulaire automata.
    Een cellulair automaton is een rekensysteem dat, gegeven een startsituatie en regels,
    in tijdstappen bepaalt of diens cellen (vakjes) "aan" of "uit" zijn ("levend" of "dood"). 
    Denk aan Conway's Game of Life [<https://playgameoflife.com/>], maar dan in één dimensie (op een lijn).
    Als we de tijdstappen verticaal onder elkaar plotten, krijgen we piramides met soms verrassend complexe patronen erin.
    LET OP: lees sowieso de informatie op [<https://mathworld.wolfram.com/Rule30.html>] en de aangesloten pagina's!
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib where

import Data.Maybe (catMaybes) -- Niet gebruikt, maar deze kan van pas komen...
import Data.List (unfoldr)
import Data.Tuple (swap)


-- ..:: Sectie 1: Basisoperaties op de FocusList ::..

-- Om de state van een cellulair automaton bij te houden bouwen we eerst een set functies rond een `FocusList` type. 
-- Dit type representeert een (1-dimensionale) lijst, met een enkel element dat "in focus" is. 
-- Het is hierdoor mogelijk snel en makkelijk een enkele cel en de cellen eromheen te bereiken.
-- Een voorbeeld van zo'n "gefocuste" lijst: 
--     [0, 1, 2, <3>, 4, 5]
-- waarbij 3 het getal is dat in focus is.
-- In Haskell representeren we de bovenstaande lijst als:
--     FocusList [3,4,5] [2,1,0]
-- waarbij het eerste element van de eerste lijst in focus is.
-- De elementen die normaal vóór de focus staan, staan in omgekeerde volgorde in de tweede lijst.
-- Hierdoor kunnen we makkelijk (lees: zonder veel iteraties te hoeven doen) bij de elementen rondom de focus,
-- en de focus makkelijk één plaats opschuiven.

data FocusList a = FocusList { forward :: [a]
                             , backward :: [a]
                             }
  deriving (Show, Eq)

-- De instance-declaraties mag je voor nu negeren.
instance Functor FocusList where
  fmap = mapFocusList

-- Enkele voorbeelden om je functies mee te testen:
intVoorbeeld :: FocusList Int
intVoorbeeld = FocusList [3,4,5] [2,1,0]

stringVoorbeeld :: FocusList String
stringVoorbeeld = FocusList ["3","4","5"] ["2","1","0"]

-- TODO: Schrijf en documenteer de functie toList, die een focus-list omzet in een gewone lijst. 
-- Het resultaat bevat geen focus-informatie meer, maar moet wel op de juiste volgorde staan.
-- Voorbeeld: toList intVoorbeeld ~> [0,1,2,3,4,5]

{-
De eerste lijst staat van de focuslist die als input is opgegeven, staat in de juiste volgorde. De tweede lijst
van de focuslist staat achterstevoren. Om de tweede lijst in de juiste volgorde te krijgen wordt mbv last
het laatste element uit de lijst gehaald. Met de : wordt het laatste element vastgeplakt aan het volgende element.
Het volgende element wordt met de recursie op de nieuwe focuslist bepaald, waarvan 
de eerste lijst onveranderd blijft, en de tweede lijst het laatste element mist. Mbv init wordt de tweede lijst geupdated.
Zodra de recursie bij lijst 2 een lege lijst detecteerd (dus FocusList fw []), dan wordt de eerste lijst (fw) aan alle elementen
vastgeplakt. Op die manier wordt een FocusList getransformeerd naar een lijst. 
-}

toList :: FocusList a -> [a]
toList (FocusList fw []) = fw
toList (FocusList fw bw) = last bw : toList (FocusList fw (init bw))

-- TODO: Schrijf en documenteer de functie fromList, die een gewone lijst omzet in een focus-list. 
-- Omdat een gewone lijst geen focus heeft moeten we deze kiezen; dit is altijd het eerste element.

{-
De focus list op het eerste element. Dit resulteert tot een list in de vorm van [<a>,b,c,d], en een focuslist in de vorm 
van FocusList [a,b,c,d] []. Een focuslist splitst een list namelijk VOOR het element waar de focus op ligt. Als de focus
op het eerste element ligt, dan blijft er een lege lijst over voor het focuselement. Dus zal de tweede lijst van de focuslist
een lege lijst worden. De eerste lijst van de focuslist moet de lijst vanaf het focuselement tot en met het einde van de lijst 
worden in dezelfde volgorde zoals ze in de lijst staan. Ofterwijl de eerste lijst van de focuslist staat gelijk aan de gehele
lijst van de list. Hieruit volgt dat de Focuslist van lijst x, getransformeerd moet worden naar een focuslist van x en [].
-}

fromList :: [a] -> FocusList a
fromList x = FocusList x []

-- Deze functie, die je van ons cadeau krijgt, schuift de focus één naar links op.
-- Voorbeeld: goLeft $ FocusList [3,4,5] [2,1,0] ~> FocusList [2,3,4,5] [1,0]
goLeft :: FocusList a -> FocusList a
goLeft (FocusList fw (f:bw)) = FocusList (f:fw) bw

-- TODO: Schrijf en documenteer de functie goRight, die de focuslist een plaats naar rechts opschuift.
{-
Wanneer de focus van een element een naar rechts opschuift, ( [1,<2>,3,4] -> [1,2,<3>,4]), dan gebeurt er het volgende.
Bij [1,<2>,3,4] hoort de FocusList [2,3,4] [1]
Bij [1,2,<3>,4] hoort de FocusList [3,4] [2,1]
De eesrte lijst van de focuslist verliest het eerste element, die vervolgens aan het begin van de tweede lijst wordt geplakt.
Mbv pattern matching wordt het eerste element van de eerste lijst (f) verplaatst naar de tweede lijst (bw). Dan blijft er alles behalve
het eerste element over van de eerste lijst (fw). 
-}
goRight :: FocusList a -> FocusList a
goRight (FocusList (f:fw) bw) = FocusList fw (f:bw)

-- TODO: Schrijf en documenteer de functie leftMost, die de focus helemaal naar links opschuift.

{- Deze functie maakt een FocusList [<x>,..] []. Dit vergt een loop door de 2e list, waarbij list 1 achter
de laatste character geplakt wordt. Als laatst blijft een lege list achter, aangezien er geen elementen
voor de left most focus liggen.
De recursie omvat de functie goLeft die de focus eentje naar links brengt totdat de basecase aangeroepen wordt.
De basecase wordt aangeroepen zodra leftMost een focusList meekrijgt waarvan de 2e list leeg is (en dus de focus helemaal
links ligt.) Vervolgens wordt de FocusList van de 1e list en een lege list gereturnd.
 -}
leftMost :: FocusList a -> FocusList a
leftMost (FocusList x []) = FocusList x []
leftMost x = leftMost $ goLeft x



-- TODO: Schrijf en documenteer de functie rightMost, die de focus helemaal naar rechts opschuift.
{- Deze functie maakt een FocusList [<x>] [y,..]. De functie rightMost fungeert hetzelfde als leftMost, met een verschil. In de recursie wordt er gebruik
gemaakt van goRight ipv goLeft. Ook verschilt deze functie met leftMost bij de base case. De base case wordt aangeroepen als er nog een element
over is in de eerste lijst. Wanneer de focus helemaal rechts licht blijft er alsnog een element over in de eerste lijst, namelijk het focuselement.
De expressie <rightMost FocusList [x] y> wordt aangeroepen bij een willekeurige y (2e lijst) en wanneer er precies een element in de eerste lijst staat 
(een willekeurige x). 
Dus wordt er gecheckt of de rechter lijst een element over heeft, aangezien de focus rechts komt te liggen,
en het "focuselement" als eerste element in list 1 staat. 
-}
rightMost :: FocusList a -> FocusList a
rightMost (FocusList [x] y) = FocusList [x] y
rightMost x = rightMost $ goRight x

-- Onze functies goLeft en goRight gaan er impliciet van uit dat er links respectievelijk rechts een waarde gedefinieerd is. 
-- De aanroep `goLeft $ fromList [1,2,3]` zal echter crashen, omdat er in een lege lijst gezocht wordt: er is niets verder naar links. 
-- Dit is voor onze toepassing niet handig, omdat we vaak de cellen direct links en rechts van de focus 
-- nodig hebben, ook als die (nog) niet bestaan.

-- Schrijf de functies totalLeft en totalRight die de focus naar links respectievelijk rechts opschuift; 
-- als er links/rechts geen vakje meer is, dan wordt een lege (dode) cel teruggeven. 
-- Hiervoor gebruik je de waarde `mempty`, waar we met een later college nog op in zullen gaan. 
-- Kort gezegd zorgt dit ervoor dat de FocusList ook op andere types blijft werken - 
-- je kan dit testen door totalLeft/totalRight herhaaldelijk op de `voorbeeldString` aan te roepen, 
-- waar een leeg vakje een lege string zal zijn.
-- NOTE: deze functie werkt niet met intVoorbeeld! (Omdat mempty niet bestaat voor Ints - maar daar komen we nog op terug!)

-- Grafisch voorbeeld: [⟨░⟩, ▓, ▓, ▓, ▓, ░]  ⤚totalLeft→ [⟨░⟩, ░, ▓, ▓, ▓, ▓, ░]

-- TODO: Schrijf en documenteer de functie totalLeft, zoals hierboven beschreven.

{-
Bij totalLeft kopieren we de functie van toRight. Voor 1 geval schrijven we een aparte functie. Wanneer 
de focus op het eerste element staat, kan de focus niet meer 1 naar links opgeschoven worden.
Daarom wordt een mempty toegevoegd aan de eerste lijst, en blijft de tweede lijst hetzelfde (aangezien
er geen elementen links van de focus liggen).
-}
totalLeft :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalLeft (FocusList fw []) = FocusList (mempty: fw) []
totalLeft (FocusList fw (f:bw)) = FocusList (f:fw) bw

-- TODO: Schrijf en documenteer de functie totalRight, zoals hierboven beschreven.
{-
Bij totalRight gebeurd in principe hetzelfde. De functie toRight wordt gekopieerd. Als de focus aan het einde van de 
list ligt, wordt het enige element in de eerste lijst van de focuslist toegevoegd aan de tweede lijst. Echter blijft nu een lege lijst over en zal bij
de volgende aanroep iets verkeerd gaan. Dus zetten we een mempty in de eerste lijst.
-}
totalRight :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalRight (FocusList [x] bw) = FocusList [mempty] (x:bw)
totalRight (FocusList (f:bw) fw) = FocusList bw (f:fw)

-- ..:: Sectie 2 - Hogere-ordefuncties voor de FocusList ::..

-- In de colleges hebben we kennis gemaakt met een aantal hogere-orde functies zoals `map`, `zipWith` en `fold[r/l]`. 
-- Hier stellen we equivalente functies voor de FocusList op.

-- TODO: Schrijf en documenteer de functie mapFocusList.
-- Deze werkt zoals je zou verwachten: de functie wordt op ieder element toegepast, voor, op en na de focus. 
-- Je mag hier gewoon map voor gebruiken.

-- ..:: Sectie 2 - Hogere-ordefuncties voor de FocusList ::..

-- In de colleges hebben we kennis gemaakt met een aantal hogere-orde functies zoals `map`, `zipWith` en `fold[r/l]`. 
-- Hier stellen we equivalente functies voor de FocusList op.

-- TODO: Schrijf en documenteer de functie mapFocusList.
-- Deze werkt zoals je zou verwachten: de functie wordt op ieder element toegepast, voor, op en na de focus. 
-- Je mag hier gewoon map voor gebruiken.
mapFocusList :: (a -> b) -> FocusList a -> FocusList b
mapFocusList func (FocusList fw bw) = FocusList (map func fw) (map func bw)

-- TODO: Schrijf en documenteer de functie zipFocusListWith.
-- Deze functie zorgt ervoor dat ieder paar elementen uit de FocusLists als volgt met elkaar gecombineerd wordt:

-- [1, 2, ⟨3⟩,  4, 5]        invoer 1
-- [  -1, ⟨1⟩, -1, 1, -1]    invoer 2
--------------------------- (*)
-- [  -2, ⟨3⟩, -4, 5    ]    resultaat

-- of, in code: zipFocusListWith (*) (FocusList [3,4,5] [2,1]) (FocusList [1,-1,1,-1] [-1]) ~> FocusList [3,-4,5] [-2]
-- Oftewel: de meegegeven functie wordt aangeroepen op de twee focus-elementen, met als resultaat het nieuwe focus-element. 
-- Daarnaast wordt de functie paarsgewijs naar links/rechts doorgevoerd, waarbij gestopt wordt zodra een van beide uiteinden leeg is. 
-- Dit laatste is net als bij de gewone zipWith, die je hier ook voor mag gebruiken.

zipFocusListWith :: (a -> b -> c) -> FocusList a -> FocusList b -> FocusList c
zipFocusListWith func (FocusList a b) (FocusList c d) = FocusList (zipWith func a c) (zipWith func b d)

-- TODO: Schrijf en documenteer de functie foldFocusList.
-- Het folden van een FocusList vergt de meeste toelichting: waar we met een normale lijst met een left fold en een 
-- right fold te maken hebben, folden we hier vanuit de focus naar buiten.
-- Vanuit de focus worden de elementen van rechts steeds gecombineerd tot een nieuw element, 
-- vanuit het element voor de focus gebeurt hetzelfde vanuit links. 
-- De twee resultaten van beide sublijsten (begin tot aan focus, focus tot en met eind) worden vervolgens nog een keer met de meegegeven functie gecombineerd. 
-- Hieronder een paar voorbeelden:

-- foldFocusList (*) [0, 1, 2, ⟨3⟩, 4, 5] = (0 * (1 * 2)) * ((3 * 4) * 5)
--                                       = (0 * 2) * (12 * 5)
--                                       = 0 * 60
--                                       = 0

-- foldFocusList (-) [0, 1, 2, ⟨3⟩, 4, 5] = (0 - (1 - 2)) - ((3 - 4) - 5)
--                                       = (0 - (-1)) - ((-1) - 5)
--                                       = 1 - (-6)
--                                       = 7

-- Je kunt, buiten de testsuite, `testFold` uitvoeren in "stack ghci" om je functie te testen.
-- Let op: de tweede lijst van de FocusList kan leeg zijn! (De eerste technisch gezien ook, maar dan heb je geen geldige FocusList.)

{-
Bij rechts associatief (foldr) geldt: a-(b-(c)). Hierbij wordt eerst b - c daarna a - (b-c) berekent. Dit is nodig voor de eerste lijst, aangezien de volgorde
van de operatie vanaf het focuselement uitgevoerd moet worden.
Op de tweede lijst moet de operatie linksassociatief (foldl) uitgevoerd worden omdat het focusrespectievelijk associatief is.
(*VERBETERING*) Echter staat de tweede lijst achterstevoren. De lijst moet dus eerst omgedraaid worden met reverse en vervolgens gebruiken we de functie 
rechts associatief (foldr) omdat de focus rechts ligt vanaf de tweede lijst in de originele lijst. 
Dus bij de eerste lijst wordt de functie links associatief gebruikt, en bij de tweede lijst wordt de lijst omgedraaid en de functie rechts associatief gebruikt.
-}
foldFocusList :: (a -> a -> a) -> FocusList a -> a
foldFocusList func (FocusList [] b) = foldr1 func $ reverse b
foldFocusList func (FocusList a []) = foldl1 func a 
foldFocusList func (FocusList a b) = func (foldr1 func (reverse b)) $ foldl1 func a


-- Testfunctie voor foldFocusList (geeft True als alles klopt, False als er één of meer niet kloppen)
testFold :: Bool
testFold = and [ foldFocusList (+) intVoorbeeld     == 15
               , foldFocusList (-) intVoorbeeld     == 7
               , foldFocusList (++) stringVoorbeeld == "012345"
               ]


-- ..:: Sectie 2.5: Types voor cellulaire automata ::..

-- Nu we een redelijk complete FocusList hebben, kunnen we deze gaan gebruiken om cellulaire automata in te ontwikkelen.
-- In deze sectie hoef je niets aan te passen, maar je moet deze wel even doornemen voor de volgende opgaven.

-- Een cel kan ofwel levend, ofwel dood zijn.
data Cell = Alive | Dead deriving (Show, Eq)

-- De onderstaande instance-declaraties mag je in dit practicum negeren.
instance Semigroup Cell where
  Dead <> x = x
  Alive <> x = Alive

instance Monoid Cell where
  mempty = Dead

-- De huidige status van ons cellulair automaton beschrijven we als een FocusList van Cells.
-- Dit type noemen we Automaton.
type Automaton = FocusList Cell

-- De standaard starttoestand bestaat uit één levende cel in focus.
start :: Automaton
start = FocusList [Alive] []

-- De context van een cel is de waarde van een cel, samen met de linker- en rechterwaarde, op volgorde.
-- Voorbeeld: de context van 4 in [1,2,3,4,5,6] is [3,4,5].
-- In de praktijk bestaat de context altijd uit drie cellen, maar dat zie je niet terug in dit type.
type Context = [Cell]

-- Een regel is een mapping van elke mogelijke context naar de "volgende state" - levend of dood.
-- Omdat er 2^3 = 8 mogelijke contexts zijn, zijn er 2^8 = 256 geldige regels.
-- Zie voor een voorbeeld [<https://mathworld.wolfram.com/Rule30.html>].
type Rule = Context -> Cell


-- ..:: Sectie 3: Rule30 en helperfuncties ::..

-- TODO: Schrijf en documenteer de functie safeHead, die het eerste item van een lijst geeft; 
-- als de lijst leeg is, wordt een meegegeven defaultwaarde teruggegeven.

{- We geven een basecase die defaultwaarde returned als lijst = []. Anders pakt die het eerste element adhv pattern matching-}
safeHead :: a        -- ^ Defaultwaarde
         -> [a]      -- ^ Bronlijst
         -> a
safeHead defaultwaarde [] = defaultwaarde
safeHead defaultwaarde (x:xs) = x

-- TODO: Schrijf en documenteer de functie takeAtLeast, die werkt als `take`, maar met een extra argument. 
-- Als de lijst lang genoeg is werkt de functie hetzelfde als `take` en worden de eerste `n` elementen teruggegeven.
-- Zo niet, dan worden zoveel mogelijk elementen teruggegeven, en wordt daarna tot aangevuld met de meegegeven defaultwaarde.
-- Voorbeelden: takeAtLeast 4 0 [1,2,3,4,5] ~> [1,2,3,4]
--              takeAtLeast 4 0 [1,2]       ~> [1,2,0,0]

{-
takeAtLeast wordt recursief aangeroepen waarbij n(aantal gewenste elementen) een wordt verlaagd totdat die 0 is. Als n=0,
dan wordt de base case aangeroepen waar het resultaat een lege lijst is. Als n>0 dan wordt het eerste element van de lijst
adhv pattern matching geconcateneerd aan de recursie van dezelfde functie (met n-=1). Als de lijst leeg is worden defaultwaarden
ipv element gekozen totdat n gelijk is aan 0.

-}
takeAtLeast :: Int   -- ^ Aantal items om te pakken
            -> a     -- ^ Defaultwaarde
            -> [a]   -- ^ Bronlijst
            -> [a]
takeAtLeast 0 defaultwaarde x = []
takeAtLeast n defaultwaarde [] = defaultwaarde : takeAtLeast (n-1) defaultwaarde ([])
takeAtLeast n defaultwaarde (x:xs) = x : takeAtLeast (n-1) defaultwaarde (xs)


-- TODO: Schrijf en documenteer de functie context, die met behulp van takeAtLeast de context van de focus-cel in een Automaton teruggeeft. 
-- Niet-gedefinieerde cellen zijn per definitie Dead.
{-
Om de context te krijgen kunnen we gebruik maken van takeAtLeast, van pattern matching en van safeHead. We voegen de functie takeAtLeast toe
op het eerste element van de 2e lijst samengevoegd met de eerste lijst, aangezien alle neighbours worden meegenomen. De safeHead is toegepast op de tweede 
lijst (y) zodat de default waarde Dead wordt gekozen als deze lijst empty is. Vervolgens worden de eerste 3 elementen eruit gepakt. 
Mocht de eerste lijst niet meer dan 1 waarde bevatten dat wordt de defaultwaarde toegekent.
Er wordt namelijk gevraagd om lege cellen als Dead te beschouwen. Als de tweede lijst leeg is, dan wordt een 'base case' aangeroepen,
waarin takeAtLeast toegepast wordt op de waarde Dead geconcaneerd op de eerste lijst. De andere functie wordt dan niet aangeroepen,
aangezien die een foutmelding geeft zodra hij een element uit een lege lijst wil halen.
-}
-- [1,2,<3>,4,5] F [3,4,5] [2,1]
context :: Automaton -> Context
context (FocusList x y)  = takeAtLeast 3 Dead ((safeHead Dead y):x)

-- TODO: Schrijf en documenteer de functie expand die een Automaton uitbreidt met een dode cel aan beide uiteindes. 
-- We doen voor deze simulatie de aanname dat de "known universe" iedere ronde met 1 uitbreidt naar zowel links als rechts.
{-
Stel de huidige automaton is op te schrijven als: [1,2,<3>4,5] of FocusList [3,4,5] [2,1]
De functie expand verbreed de automaton dan naar: [0,1,2,<3>4,5,6] of FocusList [3,4,5,6] [2,1,0]
Er komt dus een extra element/cel aan het einde van beide lijsten erbij.
Met behulp van de operator ++ kunnen lijsten aan elkaar geplakt worden. Echter moet het element Dead
Tussen haakjes staan om er een list van te maken, zodat deze operator hierop toegepast kan worden. De nieuwe focuslist wordt dus x/y ++ [Dead].
-}
expand :: Automaton -> Automaton
expand (FocusList x y) = FocusList (x ++ [Dead]) (y ++ [Dead])

-- TODO: Vul de functie rule30 aan met de andere 7 gevallen. 
-- Voor bonuspunten: doe dit in zo min mogelijk regels code. De underscore _ is je vriend.
{- Om te beginnen zoeken we de state/value combinaties:
Current Sate	111	110	101	100	011	010	001	000
Next state	   0	 0	 0	 1	 1	 1	 1	 0
We zullen de states definieren adhv pattern matching. Er wordt van rechts naar links gewerkt. De laatste state 000 is gedefinieerd als Dead.
Alle states waarvan de eerste 'bit' gelijk is aan 0/Dead, weergeeft de waarde Alive. Dus alle states dat begint met Dead is Alive. Met uitzondering
van de laatste state, maar die is al eerder gedefinieerd. Met _ wordt aangegeven dat het niet uitmaakt wat de waarden zijn voor de overige bits.
Bijna alle states die met een 1/Alive beginnen returnen een Dead value. Behalve state 100. Daarom wordt deze nog apart gedefinieerd als
Alive, en vervolgens kunnen we alle andere combinatie definieren als Dead (het maakt niet meer uit wat de state is omdat alle andere gevallen 
al eerder vastgesteld zijn).
-}
rule30 :: Rule
rule30 [Dead, Dead, Dead] = Dead 
rule30 [Dead, _,_] = Alive
rule30 [Alive,Dead,Dead] = Alive 
rule30 [_,_,_] = Dead

-- ...

-- Je kan je rule-30 functie in GHCi (voer `stack ghci` uit) testen met het volgende commando:
-- putStrLn . showPyramid . iterateRule rule30 15 $ start
-- (Lees sectie 3.5 voor uitleg over deze functies - en hoe het afdrukken nou precies werkt!)

-- De verwachte uitvoer is dan:
{-             ▓
              ▓▓▓
             ▓▓░░▓
            ▓▓░▓▓▓▓
           ▓▓░░▓░░░▓
          ▓▓░▓▓▓▓░▓▓▓
         ▓▓░░▓░░░░▓░░▓
        ▓▓░▓▓▓▓░░▓▓▓▓▓▓
       ▓▓░░▓░░░▓▓▓░░░░░▓
      ▓▓░▓▓▓▓░▓▓░░▓░░░▓▓▓
     ▓▓░░▓░░░░▓░▓▓▓▓░▓▓░░▓
    ▓▓░▓▓▓▓░░▓▓░▓░░░░▓░▓▓▓▓
   ▓▓░░▓░░░▓▓▓░░▓▓░░▓▓░▓░░░▓
  ▓▓░▓▓▓▓░▓▓░░▓▓▓░▓▓▓░░▓▓░▓▓▓
 ▓▓░░▓░░░░▓░▓▓▓░░░▓░░▓▓▓░░▓░░▓
▓▓░▓▓▓▓░░▓▓░▓░░▓░▓▓▓▓▓░░▓▓▓▓▓▓▓ -}


-- ..:: Sectie 3.5: Het herhaaldelijk uitvoeren en tonen van een cellulair automaton ::..
-- In deze sectie hoef je niets aan te passen, maar je moet deze wel even doornemen voor de volgende opgaven.

-- Een reeks van Automaton-states achtereen noemen we een TimeSeries. Effectief dus gewoon een lijst van Automatons over tijd.
type TimeSeries = [Automaton]

-- De functie iterateRule voert een regel n keer uit, gegeven een starttoestand (Automaton). 
-- Het resultaat is een reeks van Automaton-states.
iterateRule :: Rule          -- ^ The rule to apply
            -> Int           -- ^ How many times to apply the rule
            -> Automaton     -- ^ The initial state
            -> TimeSeries
iterateRule r 0 s = [s]
iterateRule r n s = s : iterateRule r (pred n) (fromList $ applyRule $ leftMost $ expand s)
  where applyRule :: Automaton -> Context
        applyRule (FocusList [] bw) = []
        applyRule z = r (context z) : applyRule (goRight z)

-- De functie showPyramid zet een reeks van Automaton-states om in een String die kan worden afgedrukt.
showPyramid :: TimeSeries -> String
showPyramid zs = unlines $ zipWith showFocusList zs $ reverse [0..div (pred w) 2]
  where w = length $ toList $ last zs :: Int
        showFocusList :: Automaton -> Int -> String
        showFocusList z p = replicate p ' ' <> concatMap showCell (toList z)
        showCell :: Cell -> String
        showCell Dead  = "░"
        showCell Alive = "▓"


-- ..:: Sectie 4: Cellulaire automata voor alle mogelijke regels ::..

-- Er bestaan 256 regels, die we niet allemaal met de hand gaan uitprogrammeren op bovenstaande manier. 
-- Zoals op de voorgenoemde pagina te zien is, heeft het nummer te maken met binaire codering. 
-- De toestand van een cel hangt af van de toestand van 3 cellen in de vorige ronde: de cel zelf en diens beide buren (de context). 
-- Afhankelijk van het nummer dat een regel heeft, mapt iedere combinatie naar een levende of dode cel.

-- TODO: Definieer de constante `inputs` die alle 8 mogelijke contexts weergeeft: [Alive,Alive,Alive], [Alive,Alive,Dead], etc.
-- Je mag dit met de hand uitschrijven, maar voor meer punten kun je ook een lijst-comprehensie of andere slimme functie verzinnen.


{- APPROACH 1
Naast gevalonderscheiding kan ook gebruik gemaakt worden met list comprehension. Dit is minder regels code en netjeser.
De mogelijke cellen zijn [Dead, Alive]
Met behulp van | (gegeven dat) en <- (for loop) kunnen alle combinaties van de cellen gemaakt worden.
Bij de eerste, tweede en derde for loop (x) wordt door de lijst [Dead, Alive] geloopt (ofterwijl de input "cells").
Hieruit volgt een combinatielijst met 2^3 mogelijkheden, omdat het een permutatie met herhaling is.

-}
inputs :: [Context]
inputs = [ [x,y,z] | x <- [Alive,Dead], y <- [Alive,Dead], z <- [Alive,Dead]]


{- APPROACH 2: oude code, maar laten staan door hoeveelheid tijd ik erin gestoken had.
alle inputs zijn in 4 gevallen op te delen:
1. een combinatie van 1 Alive en 2 Dead
2. een combinatie van 1 Dead en 2 Alive
3. alle cellen zijn Dead
4. alle cellen zijn Alive

Deze gevallen zijn apart te programmeren adhv pattern matching. 
De functie <findInputs> wordt in de functie <inputs> aangeroepen met een loop van 3 en als argument 
de state [Dead,Dead,Alive] (zie geval 1.). Vervolgens worden alle elementen 1 plek naar rechts opgeschoven
(Dead,Dead,Alive -> Alive,Dead,Dead) en wordt de functie <findInputs> opnieuw aangeroepen. Zodra n=0,
wordt gecontroleerd of de state weer Dead,Dead,Alive is (om te checken in welke fase de recursie zit). Nu wordt hetzelfde
gedaan maar dan met input state [Alive,Alive,Dead] (zie geval 2.). Nu is n gelijk aan 2, aangezien de eerste state al
vastgesteld is. Zodra de twee loops voorbij zijn wordt voor elk mogelijke invoer [_,_,_] met n=0 de state [Dead,Dead,Dead] toegevoegd.
Omdat alle andere patterns niet voldoen aan de pattern na geval 2. maakt het niet uit wat de input is. Als laatst wordt
de state [Alive,Alive,Alive] toegevoegd als alle states Dead zijn, en [] toegevoegd als alle states Alive zijn.
Deze code is net bepaald korter dan het handmatig coderen, maar het ziet er wel sick uit.
-- Het werkt alleen met input [Dead,Dead,Alive] aangezien specifieke waarden worden gecontroleerd bij het pattern matchen.
-}
{- code: 
inputs = findInputs 3 [Dead,Dead,Alive]

findInputs ::  Int -> Context -> [Context]
findInputs 0 [Alive,Alive,Alive] = []
findInputs 0 [Dead,Dead,Dead] = [Alive,Alive,Alive] : (findInputs 0 [Alive,Alive,Alive])
findInputs 0 [Dead,Dead,Alive] = [Alive,Alive,Dead] : (findInputs 2 [Dead,Alive,Alive])
findInputs 0 [_,_,_] = [Dead,Dead,Dead] : (findInputs 0 [Dead,Dead,Dead])
findInputs n [a,b,c] = [a,b,c] : (findInputs (n-1) [c,a,b])
-}

-- Deze helperfunctie evalueert met de functie (p) de waarde (x); als dit True teruggeeft, is het resultaat Just x, anders Nothing. 
guard :: (a -> Bool) -> a -> Maybe a
guard p x | p x = Just x
          | otherwise = Nothing

-- TODO: Voorzie de functie `binary` van uitgebreid commentaar.
-- Leg in dit commentaar uit: 
-- 1) Wat de functie precies doet;
-- 2) Stap voor stap, hoe de functie dat doel bereikt.
-- Tips: - Zoek de definitie van `unfoldr` op met Hoogle. 
--       - `toEnum` converteert een Int naar een ander type, in dit geval 0 -> False en 1 -> True voor Bool. 
{-
De functiecompositie wordt van achter naar voren uitgevoerd. 
Flip draait de argumenten van een functie om. Dus divMod 2 <input> wordt divMod <input> 2.
divMod geeft de modulo van een deling. Het resultaat wordt gegeven als de modulo zelf (de factor) en hoeveel er nog over blijft (de rest).
---- Dus met input 4 wordt eerst de berekening divMod 4 2 berekent waaruit volgt (2,0).
De swap functie wisselt twee argumenten simpel om. Deze twee argumenten komen uit divMod als een tuple.
De functie (/=) geeft aan of iets niet aan elkaar gelijk is. (/= (0,0)) is dus alleen False als de input hiervan ook (0,0) is. De input komt van de swap functie.
Guard weergeeft de waarde Just x als x True is. Dus als de functie (/=) True is dan returnd het een Just True. Anders returnd het Nothing.
---- Dus met input 4 wordt (2,0) met swap gewisselt naar (0,2). Dan geeft de functie (/= (0,0)) True weer, en uit de Guard volgt dan Just (0,2).
De guard functie is nu logisch. Als de waarde (0,0) is dan kan daar geen list uit gecreëerd worden met unfoldr, dus dan weergeeft het Nothing. 
unfoldr veranderd een maybe type waarin Nothing of een Just gedifinieerd is, in combi met een Int, naar een lijst met elementen volgens de Just waarde.
Dus wanneer de output van de guard (0,2) is en de input van de functie binary 10 is, dan komt er uit foldr 10,12,14,...
Vervolgens wordt repeat 0 ([0..]) aan de lijst van unfoldr geplakt. 
Vervolgens worden de eerste 8 elementen gekozen van de lijst met take 8.
De lijst van 8 elementen wordt omgedraait, en als laatst wordt er met map een functie toegepast op elk element.
Deze functie is toEnum. toEnum zorgt ervoor dat een waarde uit een enumeration wordt gereturnd gegeven de positie binnen die enumaration. 
-}
binary :: Int -> [Bool]
binary = map toEnum . reverse . take 8 . (++ repeat 0)
       . unfoldr (guard (/= (0,0)) . swap . flip divMod 2)

-- TODO: Schrijf en documenteer de functie mask, die gegeven een lijst Booleans en een lijst elementen alleen de elementen laat staan 
-- die (qua positie) overeenkomen met een True.
-- Je kunt hiervoor zipWith en Maybe gebruiken (check `catMaybes` in Data.Maybe) of de recursie met de hand uitvoeren.

{- First method: recursie. Deze methode werkt heeft een error, vandaar method 2
mask :: [Bool] -> [a] -> [a]
mask [] [] = []
mask [x] [y] = if x then [y] 
mask (x:xs) (y:ys) = if x then y : mask xs ys else mask xs ys-}

{-Second method
Eerst wordt de zipWith gebruikt om de functie guard toe te passen op de lijsten (map transform x) en de lijst y. Map transform x past de functie
transform, die is gedefinieerd in the where, toe op alle elementen van x (de boolean list). Hier komt een lijst uit met booleans. Vervolgens wordt
met guard een maybe type genereerd dat Just y (dus een element van y) is als de waarde op dezelfde plek in lijst x True als. Anders komt er Nothing uit.
Met catMaybes worden de Nothing waarden geëlimineerd zodat alleen de waarden gereturnd worden waarvan de waarde in lijst x op dezelfde positie True hebben.
-}
mask :: [Bool] -> [a] -> [a]
mask x y = catMaybes (zipWith guard (map transform x) y)
  where 
    transform a _ = a


-- TODO: Schrijf en documenteer de functie rule, die elk getal kan omzetten naar de bijbehorende regel. 
-- De Int staat hierbij voor het nummer van de regel; de Context `input` is waarnaar je kijkt om te zien of het resultaat
-- met de gevraagde regel Dead or Alive is. 
-- Tips: - Denk eraan dat het type Rule een shorthand is voor een functie-type, dus dat je met 2 argumenten te maken hebt. 
--       - Definieer met `where` een subset van `inputs` die tot een levende danwel dode cel leiden.
{-
Binary maakt van een integer een lijst met True en False waarden. Deze waarden geven aan of de inputs Dead of alive zijn. De inputs
zijn hierboven al eerder gedefinieerd, dat zijn dus alle mogelijkheden voor Dead/Alive met 3 waarden. 
Met mask worden alleen de elementen van inputs gefiltered waarvan de (binary n)-waarden True zijn. Als de input een element is van
deze inputs (inputs waarvan de boolean waarden dus True zijn), dan returnd de functie Alive. Anders is de uitkomst Dead. Dit wordt 
aangegeven met otherwise. 
-}

rule :: Int -> Rule
rule n input | input `elem` (mask (binary n) inputs) = Alive 
             | otherwise = Dead 


{- Je kunt je rule-functie in GHCi testen met variaties op het volgende commando:

   putStrLn . showPyramid . iterateRule (rule 18) 15 $ start

                  ▓
                 ▓░▓
                ▓░░░▓
               ▓░▓░▓░▓
              ▓░░░░░░░▓
             ▓░▓░░░░░▓░▓
            ▓░░░▓░░░▓░░░▓
           ▓░▓░▓░▓░▓░▓░▓░▓
          ▓░░░░░░░░░░░░░░░▓
         ▓░▓░░░░░░░░░░░░░▓░▓
        ▓░░░▓░░░░░░░░░░░▓░░░▓
       ▓░▓░▓░▓░░░░░░░░░▓░▓░▓░▓
      ▓░░░░░░░▓░░░░░░░▓░░░░░░░▓
     ▓░▓░░░░░▓░▓░░░░░▓░▓░░░░░▓░▓
    ▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓
   ▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓

   Als het goed is zal `stack run` nu ook werken met de optie (d) uit het menu; 
   experimenteer met verschillende parameters en zie of dit werkt.
-}
