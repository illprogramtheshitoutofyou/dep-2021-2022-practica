{-|
    Module      : Lib
    Description : Checkpoint voor V2DEP: decision trees
    Copyright   : (c) Nick Roumimper, 2021
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum schrijven we een algoritme om de decision tree te bouwen voor een gegeven dataset.
    Meer informatie over het principe achter decision trees is te vinden in de stof van zowel DEP als CM.
    Let op dat we hier naar een simpele implementatie van decision trees toewerken; parameters zoals maximale
    diepte laten we hier expres weg. Deze code blijft splitsen tot er geen verbetering meer mogelijk is.
    De resulterende boom zal dus "overfit" zijn, maar dat is de verwachting.
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

module Lib where

import Data.List (group, sort)

-- Allereerst definiëren we een datatype voor één enkele rij uit onze traindataset. (CRecord, voor Classification Record.)
-- Een rij uit onze dataset bestaat uit 
--     1) een lijst van numerieke eigenschappen van onze meeteenheid (properties);
--     2) een label voor de klasse waar deze meeteenheid toe behoort (label).
-- Bij het bouwen van onze boom weten we ook het label; ons doel is voor nieuwe data om op basis van de properties
-- te voorspellen welk label erbij hoort - maar dat implementeren we pas helemaal aan het eind.

data CRecord = CRecord { properties :: [Float]
                       , label :: String
                       }
  deriving (Show, Eq)

-- Onze dataset (CDataset, voor Classification Dataset) is dus simpelweg een lijst van CRecords.

type CDataset = [CRecord]

-- Bijgevoegd een paar simpele datasets, die je kunt gebruiken om mee te testen.

miniSet1 :: CDataset
miniSet1 = [CRecord [1,1] "blue", CRecord [2,2] "green", CRecord [3,3] "pink", CRecord [4,4] "purple", CRecord [5,5] "gray"]

miniSet2 :: CDataset
miniSet2 = [CRecord [1,1] "pink", CRecord [2,2] "pink", CRecord [3,3] "purple", CRecord [4,4] "blue", CRecord [5,5] "blue"]

miniSet3 :: CDataset
miniSet3 = [CRecord [1,1] "blue", CRecord [1,2] "green", CRecord [2,1] "green", CRecord [2,2] "green", CRecord [3,1] "orange", CRecord [3,2] "orange"] 


-- ..:: Sectie 1: Het bepalen van de Gini impurity ::..
-- De Gini impurity meet of de dataset rijen bevat uit maar één klasse ("puur"),
--                       of veel rijen uit allerlei verschillende klassen ("impuur").
-- Dit getal zit tussen de 0 en de 1, waar 0 zo puur mogelijk is en 1 zo impuur mogelijk.
-- Als we een splitsing maken in onze boom, willen we de Gini impurity zo laag mogelijk krijgen.

-- Bij het bepalen van de Gini impurity kijken we alleen naar de labels van de data.
-- TODO: schrijf en becommentarieer de functie getLabels die een lijst van alle labels in een dataset teruggeeft.
getLabels :: CDataset -> [String]
getLabels [] = []
getLabels (c:cd) = label c : getLabels cd

-- Om de Gini impurity te bepalen, willen we weten hoe vaak alle labels voorkomen.
-- Voorbeeld: ["a", "b", "a", "c", "c", "a"] wordt [("a", 3), ("b", 1), ("c", 2)]
-- We hebben de volgende twee hulpfuncties geïmporteerd:
--     group :: Eq a => [a] -> [[a]]
--        ^ zet alle gelijke waarden naast elkaar in een eigen lijst.
--          Voorbeeld: [1,1,2,2,2,1] => [[1,1],[2,2,2],[1]] 
--     sort :: Ord a => [a] -> [a]
--        ^ sorteert de lijst.
-- TODO: schrijf en becommentarieer de functie countLabels die telt hoe vaak alle labels in de dataset voorkomen.
{-
Eerst wordt er een gesorteerde en gegroepeerde lijst met labels gemaakt van de cdataset. (eerst sort dan group).
Dan wordt een lambda functie toegepast op elk element uit de gegroupeerde list. De lambda functie genereert een tuple
van het eerste element en de lengte van een list.
-}
countLabels :: CDataset -> [(String, Int)]
countLabels cd = map (\x -> (head x, length x)) (group (sort (getLabels cd)) )

-- Voor toekomstig gebruik willen we alvast een functie hebben die het meest voorkomende label
-- van een dataset geeft. Bij gelijkspel mag je eender welk teruggeven.
-- TODO: schrijf en becommentarieer de functie mostFrequentLabel op basis van countLabels.
-- HINT: gebruik een functie uit de Prelude. We gebruiken mostFrequentLabel pas in de laatste sectie!
{-
countLabels gives [(str,int),(str,int),...]
ipv een recursieve externe functie wordt er nu vooral gebruik gemaakt van lambda functies.
Eerst worden de tuples van countLabels omgedraaid door een lambda functie met map op alle tuples toe te passen.
Vervolgens wort er gesorteerd op het eerste element (dus hoevaak een label voorkomt) (dit is wel ascending)
Omdat het ascending is wordt het laatste element opgevraagd.
De tuple moet nog unpacked worden dus wordt weer een lambda functie toegepast om de tuple te unpacken.
-}

mostFrequentLabel :: CDataset -> String
mostFrequentLabel cd = (\(x,y) -> y) $ last $ sort $ map (\(x,y) -> (y,x)) (countLabels cd)

-- We definiëren de volgende hulpfunctie (fd, voor "Float Division") om twee Ints te delen als twee Floats.
-- Voorbeeld: fd 3 4 ~> 0.75 (een Float), i.p.v. 0 (een Int).
fd :: Int -> Int -> Float
fd x y = (/) (fromIntegral x) (fromIntegral y)

-- De Gini impurity van één dataset is:
--     de som van de kansen voor elke klasse dat
--         ik, uit alle rijen, willekeurig een rij uit die klasse trek én
--         ik, uit alle rijen, willekeurig een rij uit een andere klasse trek.
-- Zie ook de stof van CM en de Canvas van DEP voor meer context.
-- TODO: schrijf en becommentarieer de functie gini die de Gini impurity van één dataset bepaalt.

{-
gini = 1 - summation of chances existence of classes squared. So 1-som(Pi^2).
To calculate Pi, we first call the function countLabels, this gives us a list with how many labels exist in the CDataset.
With the function chance we recursively loop through all labels and we calculate the proportion of the amount of labels (n)
to all labels (length of de dataset cd). The summation of squaring these values gives us almost the endresult. Now we only 
substract this number from 1 to get the impurity gini.
edit: now the recursive helper function has been replaced with a lambda function mapped over de countLabels.
Foldl1 is used to calculate the sum of all ginis for each label.
-}
gini :: CDataset -> Float
gini cd = (-) 1 $ foldl1 (+) (map (\(str,n) -> (fd n (length cd))^2) $ countLabels cd)

-- gini :: CDataset -> Float
-- gini cd = (-) 1 $ chance cd $ countLabels cd
-- chance cd [] = 0
-- chance cd ((str,n):lbls) = (fd n (length cd))^2 + chance cd lbls

-- De gecombineerde Gini impurity van twee datasets (in ons geval: na een splitsing)
-- is de gewogen som van de Gini impurity van beide sets.
-- Voorbeeld: mijn splitsing leidt tot
--     1) een dataset met 3 rijen en een Gini impurity van 0.2;
--     2) een dataset met 2 rijen en een Gini impurity van 0.1.
-- Dan is de gecombineerde Gini impurity (0.2 * (3/5)) + (0.1 * (2/5)) = 0.16.
-- TODO: schrijf en becommentarieer de functie giniAfterSplit die de gecombineerde Gini impurity van twee datasets bepaalt.

{-
We devide the length of each dataset by the length of both datasets combined. We multiply the gini of both datasets to their
correspondending proportions, to get the weighted gini, and add both values together. In order to divide de lenth of datasets
we have to use the predifined function <fd>. The length of anything always returns an Integer, and in order to do division we 
need to transform this value to a Float. 
edit: now the where function avoids rendancy in the code.
-}
giniAfterSplit :: CDataset -> CDataset -> Float
giniAfterSplit cd1 cd2 = f cd1 cd2 + f cd2 cd1
    where f cd1 cd2 = gini cd1 * (fd (length cd1) (length cd1 + length cd2))


-- ..:: Sectie 2 - Het genereren van alle mogelijke splitsingen in de dataset ::..
-- Bij het genereren van onze decision tree kiezen we telkens de best mogelijke splitsing in de data.
-- In deze simpele implementatie doen we dat brute force: we genereren alle mogelijke splitsingen
-- in de data, en checken ze allemaal. Hier beginnen we door de splitsingen te genereren.

-- We slaan elke mogelijke splitsing op in het datatype CSplit (voor Classification Split). Deze bestaat uit:
--     1) de eigenschap waarop gesplitst wordt, opgeslagen als de index in de lijst van properties (feature);
--     2) de waarde van deze feature waarop we splitsen - ofwel kleiner-gelijk-aan, ofwel groter dan (value).
-- Let op: feature refereert aan de positie in de lijst van properties van een CRecord.
-- Oftewel: als we het hebben over feature 1 van CRecord [8.0, 5.0, 3.0] "x", bedoelen we 5.0.
data CSplit = CSplit { feature :: Int
                     , value :: Float   
                     }
    deriving (Show, Eq, Ord)

-- Allereerst willen we alle waarden van een bepaalde feature in een aparte lijst hebben.
-- TODO: schrijf en becommentarieer de functie getFeature, die gegeven een feature (index in de lijst properties) en een dataset,
--       een lijst teruggeeft van alle waarden van die feature.

{-
oud:
Er is een extra functie aangemaakt <locateFeature> om met recursie het juiste element op index=feature op te vragen. Hierbij
wordt de functie steeds aangeroepen op de tail van de opgegeven list waarbij Feature (n) steeds met 1 vermindert. Als n=0,
dan wordt het eerste element als uitkomst gegeven. (dit is de base case)
Deze fucntie wordt aangeroepen op de properties van de CRecords. Met recursie wordt de head genomen van de CDataset, en vervolgens
de head genomen van de tail van de CDataset. Op die manier worden elementen van alle CRecords aan elkaar geplakt.
edit: 
Inplaats van een extra recursieve functie is gebruik gemaakt van map en lambda calculus.
Eerst worden de properties van de Cdataset opgevraagd door met map de properties toe te passen op alle Crecords in Cdataset.
Vervolgens wordt een anonieme functie aangeroepen op elke Crecord. Deze functie vraagt het element op in een lijst gegeven een index.
Met map worden alle properties opgezocht gegeven een feature index.
-}
getFeature :: Int -> CDataset -> [Float]
getFeature n cd  = map (\x -> x!!n) (map properties cd)

-- Als we een lijst van waarden hebben, hoeven we alleen naar de unieke waarden te kijken.
-- Tegelijkertijd is het wel zo makkelijk als de unieke waarden alvast zijn gesorteerd.
-- TODO: schrijf en becommentarieer de functie getUniqueValuesSorted, die uit een lijst van Floats de unieke waarden gesorteerd teruggeeft.
-- HINT: gebruik de hulpfuncties uit de vorige sectie.

{-
We add a function <findUniques> that deals with duplicates. The main function only has to sort the list.
findUniques requires an old value as argument, so for the fist recursion loop we chose something that cant be possibly
the same as the first element of the given list. (Because it checks if the first elements equals the old value).
So we simply take the first argument of values, and add anything to make it different from the first element.
-}


{-
With function composition we can combine group and sort and head in order to get the first element of each group in a sorted list.
-}

getUniqueValuesSorted :: [Float] -> [Float]
getUniqueValuesSorted values = (map head . group . sort) values

-- Als we de dataset splitsen, doen we er verstandig aan om niet precies op een waarde uit de dataset te splitsen.
-- In plaats daarvan splitsen we op het gemiddelde van alle twee naast elkaar gelegen waarden.
-- Voorbeeld: getAverageValues [2.0, 3.0, 5.0, 9.0] ~> [2.5, 4.0, 7.0]
-- Voor de traindata maakt dat geen verschil, maar voor het voorspellen van nieuwe waarden wel.
-- TODO: schrijf en becommentarieer de functie getAverageValues, die de gemiddelden bepaalt van alle paren van twee waarden in een lijst.
{-
In order to get average values from a list of floats we loop with recursion through the function. With pattern matching we 
can determine the first element of the list, and th second element of the list (head of the tail of the list). We can calculate
the average from those two values and combine all averages into a list with help of the base case. The base case returns an empty 
list if only one element is left. 
-}
getAverageValues :: [Float] -> [Float]
getAverageValues [x] = []
getAverageValues (x:xs) = ( (/) (x + (head xs)) 2 ) : getAverageValues xs

-- Met deze functies kunnen we alle mogelijke CSplits bepalen voor één gegeven feature.
-- Voorbeeld: een dataset met in feature 2 de waarden [9.0, 2.0, 5.0, 3.0] wordt
--            [CSplit 2 2.5, CSplit 2 4.0, CSplit 7.0].
-- TODO: schrijf en becommentarieer de functie getFeatureSplits, die alle mogelijke CSplits bepaalt voor een gegeven feature.

{-
Om alle feature splits te krijgen wordt een lambda functie toegepast waarbij van elke feature de featuresplits wordt bepaald.
Deze functie wordt toegepast op een lijst die van 0 tot de lengte van de properties van de eerste record loopt. (Note: lengte van
de properties zijn voor elke record gelijk). Vervolgens worden de lijsten van de featuresplits aan elkaar geplakt met foldl1 (++).
-}
getAllFeatureSplits :: CDataset -> [CSplit]
getAllFeatureSplits cd = foldl1 (++) $ map (\x -> getFeatureSplits x cd) $  [0..(length (properties (head cd)) -1)]



-- ..:: Sectie 3 - Het vinden van de beste splitsing ::..
-- Nu we alle splitsingen hebben gegenereerd, rest ons nog de taak de best mogelijke te vinden.
-- Hiervoor moeten we eerst de functies schrijven om één CDataset, op basis van een CSplit,
-- te splitsen in twee CDatasets.

-- Allereerst schrijven we de functie waarmee we bepalen in welke dataset een CRecord belandt
-- gegeven een bepaalde splitsing. Deze functie moet True teruggeven als de waarde van de feature  
-- kleiner-gelijk-aan is aan de splitswaarde, en False als deze groter is dan de splitswaarde.
-- Voorbeelden: gegeven CSplit 1 3.0 en CRecord [4.0, 2.0, 9.0] "x", is het resultaat True.
--              gegeven CSplit 1 1.0 en CRecord [4.0, 2.0, 9.0] "x", is het resultaat False.
-- TODO: schrijf en becommentarieer de functie splitSingleRecord, die voor een enkel CRecord True of False teruggeeft.

{-
Als allereerst halen we de properties op van de CRecord. Vervolgens checken we of de waarde op index = feature (opgevraagd vanuit
de CSplit) lager/gelijk of hoger is dan de split waarde. Een element uit een lijst halen kan met (!!), maar voor de volledigheid
is een functie aangemaakt <getPropertiesElement> die recursief elk element af gaat waarbij feature telkes een omlaag gaat.
De base case geeft het element op index = feature. 
Edit: Nu wordt er met pattern matching simpelweg het element opgevraagd uit de properties met de feature index uit CSplit.
Als de waarde kleiner of gelijk is aan de splitswaarde in de CSplit, dan geeft dit een True waarde terug. Met <= wordt dit direct
bereikt. De label van CRecord is niet nodig dus die negeren we met _.
-}
splitSingleRecord :: CSplit -> CRecord -> Bool
splitSingleRecord (CSplit feature value) (CRecord prop _) = (prop!!feature) <= value


-- Nu kunnen we de functie schrijven die één dataset opsplitst in twee, op basis van een CSplit object.
-- TODO: schrijf en becommentarieer de functie splitOnFeature, die één dataset opsplitst in twee.
-- HINT: gebruik een functie uit de Prelude. Onthoud dat CDataset = [CRecord]!

-- https://stackoverflow.com/questions/16247098/writing-a-recursive-function-with-condition-in-haskell
{-
Met hulp van filter worden externe functies gemapt over de CDataset.
Below geeft een true waarde als een CRecord onder de CSplit grens zit
en Above geeft een true waarde als een CRecord boven de CSplit grens zit.
De return waarde is een tuple van alle waarden gefilterd onder de CSplit
en van alle waarden gefilterd boven de CSplit.
-}
splitOnFeature :: CDataset -> CSplit -> (CDataset, CDataset)
splitOnFeature cd cs = (filter below cd, filter above cd)
    where
        below cr = splitSingleRecord cs cr
        above cr = not $ splitSingleRecord cs cr





-- Nu kunnen we:
--     1) alle splitsingen genereren voor een CDataset, met behulp van Sectie 2;
--     2) de datasets die resulteren bij elk van die splitsingen genereren.
-- Wel is het van belang dat we onthouden welke splitsing bij welke twee datasets hoort.
-- TODO: schrijf en becommentarieer de functie generateAllSplits, die voor een gegeven dataset alle mogelijke splitsingen "uitprobeert".
{-
Eerst wordt een anonieme functie toegepast op alle featureSplits. Deze functie maakt een tuple van de split en van 
de gesplitte datasets. De gesplitte datasets staan als aparte tuple geformatteerd dus wordt de eerste lambda functie
toegepast op alle waarden zodat de tuple niet meer nested is.
-}
generateAllSplits :: CDataset -> [(CSplit, CDataset, CDataset)]
generateAllSplits cd = map (\(x,(y,z)) -> (x,y,z)) (map (\x -> (x, splitOnFeature cd x)) (getAllFeatureSplits cd))


-- De laatste stap van deze sectie combineert Sectie 1 en Sectie 3:
--     1) Genereer alle mogelijke splits;
--     2) Bepaal welke van deze splitsingen het beste resultaat geeft - oftewel, de laagste Gini impurity.
-- Hierbij willen we graag zowel de Gini impurity als de splitsing zelf onthouden.
-- TODO: schrijf en becommentarieer de functie findBestSplit, die voor een dataset de best mogelijke splitsing vindt.
-- HINT: gebruik een functie uit de Prelude. Hoe werkt "kleiner dan" voor tupels?
{-
Om de beste split te vinden worden eerst alle splits gegenereerd. Vervolgens wordt een lambda functie toegepast op alle splits.
Deze functie berekent de gini van de twee datasets van de splits, en maakt een tuple van de gini en de CSplit.
Deze functie wordt gemapt over alle plits. Vervolgens worden de tuples gesorteerd (waarbij alleen naar het eerste 
element gekeken wordt.) Het eerste element is de tuple van de gini en de CSplit met de laagste gini.
-}
findBestSplit :: CDataset -> (Float, CSplit)
findBestSplit cd = head $ sort $ map (\(cs,cd1,cd2) -> (giniAfterSplit cd1 cd2, cs)) $ generateAllSplits cd

-- ..:: Sectie 4 - Genereren van de decision tree en voorspellen ::..
-- In deze laatste sectie combineren we alle voorgaande om de decision tree op te bouwen,
-- en deze te gebruiken voor voorspellingen.

-- We introduceren het datatype van onze boom, de DTree (Decision Tree).
-- In de DTree is sprake van twee opties:
--     1) We hebben een blad van de boom bereikt, waarin we een voorspelling doen van het label (Leaf String);
--     2) We splitsen op een bepaalde eigenschap, met twee sub-bomen voor <= en > (Branch CSplit DTree DTree).
-- Zoals je al ziet is de definitie van Branch CSplit DTree DTree recursief; er kan dus een onbepaald aantal
-- vertakkingen zijn, maar uiteindelijk eindigt elke vertakking in een blad (Leaf).
-- Let op: we onthouden niet de records uit de dataset, maar wel waarop we ze gesplitst hebben (CSplit)!
data DTree = Branch CSplit DTree DTree | Leaf String deriving (Show, Eq, Ord)

-- De logica achter het recursief bouwen van een decision tree is als volgt:
--     ALS de Gini impurity van de dataset 0.0 is (perfect gesplitst)
--     OF de Gini impurity wordt zelfs met de best mogelijke splitsing niet beter
--         DAN geef ik een Leaf terug met daarin het vaakst voorkomende label;
--     ZO NIET,
--         DAN geef ik een Branch terug met daarin de best mogelijke splitsing
--         en de decision trees (sub-bomen) op basis van de twee datasets na die splitsing.
-- TODO: schrijf en becommentarieer de functie buildDecisionTree.
{-
BuildDecisionTree stopt wanneer de gini 0 is of wanneer de gini lager is dan de gini van de volgende beste split.
In dat geval wordt de laatste leaf gecreëeerd met het meest voorkomende label. 
Als dat niet het geval is dan wordt een nieuwe branch gemaakt. Een branch bestaat uit de csplit en twee DTrees.
De beste split wordt gevonden met findBestSplit, dit fungeert als het eerste element van de Branch.
Daarna worden twee nieuwe DTrees gemaakt van de dataset onder de CSplit en boven de CSplit. De splittedDatasets staan
in de where functie gedefinieerd. Het verkrijgen van het eerste en het tweede element uit een tuple staan hier ook.
-}
buildDecisionTree :: CDataset -> DTree
buildDecisionTree cd | (gini cd == 0) || (gini cd <= first (findBestSplit cd)) = Leaf (mostFrequentLabel cd)
                     | otherwise                  = Branch (last (findBestSplit cd)) (buildDecisionTree (first (splittedDatasets))) (buildDecisionTree (last splittedDatasets))
    where 
        first (x,y)= x
        last (x,y) = y
        splittedDatasets = splitOnFeature cd (last (findBestSplit cd))


-- Tot slot, bij het voorspellen weten we alleen de eigenschappen ([Float]), niet het label.
-- TODO: schrijf en becommentarieer de functie predict, die op basis van een boom en de gegeven eigenschappen het label voorspelt.
{-
Er wordt elke keer gecheckt of de propertie op positie van de CSplit hoger is of lager is dan de waarde
van de CSplit. Als die lager is dan valt de waarde onder cd1, anders onder cd2. Als een leaf is bereikt, dan
is de uiteindelijke voorspelling bereikt en wordt de recursie afgesloten. Bij de base case negeren we de input 
waarden van de voorspelling omdat die niet meer van belang is.
-}
predict :: DTree -> [Float] -> String
predict (Leaf l) _ = l
predict (Branch (CSplit feature value) cd1 cd2) prop | (prop!!feature) <= value = predict cd1 prop
                                                     | otherwise                = predict cd2 prop


